from django.conf import settings

from django.contrib import admin
from django.urls import path, include
from django.views import defaults as default_views
from django.views.generic import RedirectView


urlpatterns = [
    path("", RedirectView.as_view(pattern_name='payments:order_list', permanent=False), name="home"),
    # Django Admin, use {% url 'admin:index' %}
    path("admin/", admin.site.urls),
    # Payment System
    path("payments/", include("payments.urls", namespace="payments")),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
