import uuid

from django.db import models
from django.core.validators import MinValueValidator

from decimal import Decimal
from datetime import datetime


class OrderStatus(models.Model):
    """Status of order"""
    name = models.CharField("Status of Order", max_length=30, unique=True)

    class Meta:
        verbose_name = 'Order Statuse'

    def __str__(self):
        return self.name


class Order(models.Model):
    """Provide model for orders"""

    # ID orders are safer to store in UUID
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    # Deposit management info
    created_at = models.DateTimeField('Creation date', auto_now_add=True)
    purporse = models.CharField("Purpose of payment", max_length=30)
    status = models.ForeignKey(OrderStatus, related_name="orders",
                               on_delete=models.SET_NULL, null=True, blank=True)

    # Transaction info
    amount = models.DecimalField(
        'Amount', max_digits=12, decimal_places=2,
        validators=[MinValueValidator(Decimal('0.01'))])

    class Meta:
        verbose_name = 'Payment Order'
        ordering = ('-created_at', )

    def __str__(self):
        return f'Deposit {self.id}: {self.amount} {self.status}'

