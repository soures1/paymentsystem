from django.contrib import admin
from payments.models import Order, OrderStatus

# Register your models here.
admin.site.register(Order)
admin.site.register(OrderStatus)