from payments.models import Order, OrderStatus
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView, DeleteView, CreateView


# Create your views here.
class OrderListView(ListView):
    """List view for Order model"""
    template_name = "payments/payment_list.html"
    model = Order

class OrderCreateView(CreateView):
    """Create order view"""
    template_name = "payments/create_form.html"
    fields = ('purporse', 'status', 'amount')
    success_url = reverse_lazy('payments:order_list')
    model = Order

class OrderUpdateView(UpdateView):
    """Update order fields"""
    template_name = "payments/update_form.html"
    fields = ('purporse', 'status', 'amount')
    success_url = reverse_lazy('payments:order_list')
    model = Order

class OrderDeleteView(DeleteView):
    """Delete order"""
    template_name = "payments/delete_form.html"
    model = Order
    success_url = reverse_lazy('payments:order_list')


class StatusListView(ListView):
    """List view for status model"""
    template_name = "payments/status_list.html"
    model = OrderStatus


class StatusCreateView(CreateView):
    """Create status view"""
    template_name = "payments/create_form.html"
    fields = ('name', )
    success_url = reverse_lazy('payments:status_list')
    model = OrderStatus


class StatusUpdateView(UpdateView):
    """Update status fields"""
    template_name = "payments/update_form.html"
    fields = ('name', )
    success_url = reverse_lazy('payments:status_list')
    model = OrderStatus


class StatusDeleteView(DeleteView):
    """Delete status"""
    template_name = "payments/delete_form.html"
    model = OrderStatus
    success_url = reverse_lazy('payments:status_list')
