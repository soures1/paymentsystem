from payments.views import (StatusListView, StatusCreateView, StatusDeleteView, StatusUpdateView,
                            OrderUpdateView, OrderDeleteView, OrderCreateView, OrderListView)
from django.urls import path


app_name = "payments"
urlpatterns = [
    path("order/", view=OrderListView.as_view(), name="order_list"),
    path("order/create/", view=OrderCreateView.as_view(), name="order_create"),
    path("order/update/<pk>/", view=OrderUpdateView.as_view(), name="order_update"),
    path("order/delete/<pk>/", view=OrderDeleteView.as_view(), name="order_delete"),
    path("status/", view=StatusListView.as_view(), name="status_list"),
    path("status/create/", view=StatusCreateView.as_view(), name="status_create"),
    path("status/update/<pk>/", view=StatusUpdateView.as_view(), name="status_update"),
    path("status/delete/<pk>/", view=StatusDeleteView.as_view(), name="status_delete"),

]