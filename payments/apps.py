from django.apps import AppConfig


class PaymentsConfig(AppConfig):
    name = 'payments'
    verbose_name = "Payments"

    def ready(self):
        try:
            import payments.signals  # noqa F401
        except ImportError:
            pass